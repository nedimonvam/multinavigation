package de.appsfactory.navsample.ui.navigation

sealed class NavAction {
    class ToFeatureOne(val message: String) : NavAction()
}