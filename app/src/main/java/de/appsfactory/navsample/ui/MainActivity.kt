package de.appsfactory.navsample.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import de.appsfactory.navsample.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
