package de.appsfactory.navsample.ui.navigation

import androidx.navigation.NavController
import de.appsfactory.navsample.ui.HomeFragmentDirections

class NavHandler(
    val navController: NavController
) {

    fun navigate(navAction: NavAction) = when (navAction) {
        is NavAction.ToFeatureOne -> navController.navigate(
            HomeFragmentDirections.actInternalFirst(navAction.message)
        )
    }
}