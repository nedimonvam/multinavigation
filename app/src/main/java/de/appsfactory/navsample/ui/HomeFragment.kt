package de.appsfactory.navsample.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import de.appsfactory.navsample.databinding.FragmentHomeBinding
import de.appsfactory.navsample.ui.navigation.NavAction
import de.appsfactory.navsample.ui.navigation.NavHandler
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class HomeFragment : Fragment() {

    private val navHandler: NavHandler by inject { parametersOf(findNavController()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        navHandler.navigate(NavAction.ToFeatureOne("test"))
    }
}