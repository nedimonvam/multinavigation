package de.appsfactory.navsample

import android.app.Application
import androidx.navigation.NavController
import de.appsfactory.navsample.ui.navigation.NavHandler
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules (
                module {
                    single { (navController: NavController) -> NavHandler(navController) }
                }
            )
        }
    }
}